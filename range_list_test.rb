# frozen_string_literal: true

require 'test/unit'
require_relative './range_list'

# RangeList test
class RangeListTest < Test::Unit::TestCase

  def test_add_one_range
    r = RangeList.new
    r.add([1, 5])
    assert_equal(' [1, 5)', r.to_s)
  end

  def test_add_two_range
    r = RangeList.new
    r.add([1, 5])
    r.add([10, 20])
    assert_equal(' [1, 5) [10, 20)', r.to_s)
  end

  def test_add_empty_range
    r = RangeList.new
    r.add([1, 5])
    r.add([5, 5])
    assert_equal(' [1, 5)', r.to_s)
  end

  def test_add_range_contains
    r = RangeList.new
    r.add([1, 5])
    r.add([2, 4])
    assert_equal(' [1, 5)', r.to_s)
  end

  def test_remove_empty_range
    r = RangeList.new
    r.add([1, 5])
    r.remove([5, 5])
    assert_equal(' [1, 5)', r.to_s)
  end

  def test_remove_range
    r = RangeList.new
    r.add([1, 10])
    r.remove([3, 7])
    assert_equal(' [1, 3) [7, 10)', r.to_s)
  end

  def test_remove_range_three_part
    r = RangeList.new
    r.add([1, 5])
    r.add([10, 17])
    r.remove([11, 14])
    assert_equal(' [1, 5) [10, 11) [14, 17)', r.to_s)
  end

  def test_add_nil
    r = RangeList.new
    r.add(nil)
    assert_equal('', r.to_s)
  end

  def test_add_not_valid_range
    r = RangeList.new
    r.add([19])
    assert_equal('', r.to_s)
  end

  def test_remove_nil
    r = RangeList.new
    r.add([1, 5])
    r.remove(nil)
    assert_equal(' [1, 5)', r.to_s)
  end

  def test_remove_not_valid_range
    r = RangeList.new
    r.add([1, 5])
    r.remove(nil)
    assert_equal(' [1, 5)', r.to_s)
  end

end
