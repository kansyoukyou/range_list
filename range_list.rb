# frozen_string_literal: true

require 'set'

# RangeList implemented by sortedSet
class RangeList

  def initialize
    @collection = SortedSet.new
  end

  def add(range)
    to_range(range) { |i| @collection.add(i) }
  end

  def remove(range)
    to_range(range) { |i| @collection.delete(i) }
  end

  def print
    puts to_s
  end

  def to_s
    arr = @collection.to_a
    ans = ''

    left = 0
    right = -1

    while (right < arr.size) && (left < arr.size)
      right += 1 while (right + 1 < arr.size) && same_step(arr, left, right)
      ans += " [#{arr[left]}, #{arr[right] + 1})"

      left = right + 1
    end

    ans
  end

  private

  def to_range(range, &block)
    return if range.nil? || !range.is_a?(Array) || range.empty? || range.length < 2

    left, right = range
    (left...right).each do |item|
      block.call(item)
    end
  end

  def same_step(arr, left, right)
    arr[right + 1] - arr[left] == right + 1 - left
  end

end
